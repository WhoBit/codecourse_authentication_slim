<?php

namespace App\Controllers;

class Controller
{
    protected $container;
    
    public function __construct($container)
    {
        $this->container = $container;
        
#        return $this->view->render($response, 'home.twig');
    }

    public function __get($property)
    {

        if ($this->container->{$property}){
            return $this->container->{$property};
        }

    }

}
