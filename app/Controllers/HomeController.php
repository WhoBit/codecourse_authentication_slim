<?php

namespace App\Controllers;

use App\Models\User;

class HomeController extends Controller
{    
    public function index($request, $response)
    {
        /*
        User::create([
            'name' => 'Grizmo',
            'email' => 'grizzle@gmail.com',
            'password' => '123',
        ]);
         * 
         */
        
        //$this->flash->addMessage('error', 'Test flash message');
        
        return $this->view->render($response, 'home.twig');
    }
}
