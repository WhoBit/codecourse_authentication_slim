<?php

namespace App\Auth;

use App\Models\User;


class Auth
{
    public function user()
    {
        if (isset($_SESSION['user'])){
            return User::find($_SESSION['user']);
        }
        
        return false;
        
    }
    public function check()
    {

        return isset($_SESSION['user']);
       
        
    }
    public function attempt($email, $password)
    {
        $user = User::where('email', $email)->first();
        #$this->logger->addInfo("Loggin attempt here...");
        if (!$user) {
            #$this->logger->addInfo("Loggin attempt failed");
            return false;
        }
        #$this->logger->addInfo("Loggin attempt succeeded");
        if (password_verify($password, $user->password)) {
            $_SESSION['user'] = $user->id;
            #$this->logger->addInfo("Session user defined" . $_SESSION['user']);
            return true;
        }
        #$this->logger->addInfo("Loggin attempt failed due to password");        
        return false;
    }
    public function logout()
    {
        unset($_SESSION['user']);
    }    
}



