<?php

namespace \App\Validation;

use Respect\Validation\Validator as Respect;
use Respect\Validation\Exceptions\NestedValidationException;

class Validator
{
    protected $errors;
    
    public function validate($request, array $rules)
    {
        
        foreach ($rules as $field => $rule){
            try {
                $rule->setName(ucfirst($field))->asert($request->getParam($field));
            } catch (NestedValidationException $ex) {
                $this->errors[$field] = $ex->getMessages();
            }
            
        }
        var_dump($this->errors);
        die();
    }
    
}