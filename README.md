I have been studying at codecourse.com and have just finished the ["Authentication with Slim 3"](https://www.codecourse.com/library/lessons/slim-3-authentication/setting-up-slim) course so thought I would put the code here.

Not trying to say that I wrote this code myself.  I just typed it in as I followed along with this course. But, I did follow along and understand the material (for the most part) and it did work when I was done.

Another step as I am continuing to study and learn PHP.

-kg